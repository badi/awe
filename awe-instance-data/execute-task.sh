#!/usr/bin/env bash

source env.sh

prelude
check-initial
run-md
assign
check-result
package
cleanup
